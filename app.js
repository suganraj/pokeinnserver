// ./express-server/app.js
require('rootpath')();
import express from 'express';
import path from 'path';
import logger from 'morgan';
import mongoose from 'mongoose';
import SourceMapSupport from 'source-map-support';
import bb from 'express-busboy';
import route from './route';
import mongoStoreFactory from "connect-mongo";



var cors = require('cors');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var expressJwt = require('express-jwt');
var config = require('config.json');
var jwt = require('jsonwebtoken');
var schedule = require('node-schedule');
const passport = require('passport');
import Currencies from './models/currencies.model';
var request = require('request');
// var ua = require('universal-analytics');
// var visitor = ua('UA-124556114-1' , {http: true});


//f5766de088ffc7bf672934a03f8a1695
//f59a81350921dd481d2c1f228fd6d84e

var j = schedule.scheduleJob('59 * * * *', function() {
    request('http://data.fixer.io/api/latest?access_key=f5766de088ffc7bf672934a03f8a1695', function(error, response, body) {
        if (body) {
            var currencydata = JSON.parse(body);
            if (currencydata.success) {
                Currencies.findOneAndUpdate({ _id: '5c9f08e3d2fb462514d772df' }, currencydata, { new: true }, (err, curency) => {
                    if (err) {
                        console.log(err);
                    } else {}
                })
            }
        }
    });
});

import Bookings from './models/bookings.server.model';
var i = schedule.scheduleJob('* * * * 1', function() {
    var todaydate = new Date();
    var searchInput = {
        bookedto: { $lte: todaydate },
        tripstatus: 'pending'
    };
    var bookingData;

    Bookings.find(searchInput).lean().exec(function(err, bookings) {
        if (err) return handleError(err);
        else {
            bookings.forEach(function(data) {
                bookingData = data;
                if (bookingData.payment === 'completed' && bookingData.hostapproval === 'accepted') {
                    updatebookingtrip({ tripstatus: 'completed' })
                } else {
                    updatebookingtrip({ tripstatus: 'expired' });
                }
            });
        }
    });

    function updatebookingtrip(updatedata) {
        Bookings.findOneAndUpdate({ _id: bookingData._id }, updatedata, { new: true }, (err, val) => {
            if (err) {
                console.err(err)
            } else {}
        })
    }

});




// define our app using express
const app = express();
// app.set('superSecret', config.secret);

app.use(cors());
app.use(logger('dev'));

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('uploads'));
app.use(express.static('assets/images'));


// app.use(ua.middleware("UA-124556114-1", {cookieName: '_ga'}));

// connect to database
mongoose.Promise = global.Promise;
mongoose.connect(config.connectionString, {
    useMongoClient: true,
});


// allow-cors
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    // allow preflight
    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
});


// configure app

// use JWT auth to secure the api, the token can be passed in the authorization header or querystring
// app.use(expressJwt({
//     secret: config.secret,
//     getToken: function (req) {
//         if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
//             return req.headers.authorization.split(' ')[1];
//         } else if (req.query && req.query.token) {
//             return req.query.token;
//         }
//         return null;
//     }
// }).unless({ path: config.exceptionPath }));



// const MongoFactory = mongoStoreFactory(session);
app.use(session({
    secret: 'aitravelapp',
    resave: false,
    saveUninitialized: true,
    cookie: {
        // secure: true,
        path: '/',
        maxAge: 1800000
    },
    store: new MongoStore({ mongooseConnection: mongoose.connection, ttl: (1 * 60 * 60) }),
    name: "id001"
}));


session.Session.prototype.login = function(user, cb) {
    const req = this.req;
    req.session.regenerate(function(err) {
        if (err) {
            cb(err);
        }
    });
    req.session.userInfo = user;
    cb();
};

require('./config/passport.config.js');

app.use(passport.initialize());

// app.use(passport.session()); 


// add Source Map Support
SourceMapSupport.install();


app.use('/users', require('./controllers/users.controller'));
// app.use('/users', require('./route'));

app.use('/route/', route);
app.use(function(req, res, next) {

    next();
})


// Access the session as req.session
// app.get('/', function(req, res, next) { 
//     if (req.session.views) {
//       req.session.views++ 
//       res.setHeader('Content-Type', 'text/html')
//       res.write('<p>views: ' + req.session.views + '</p>')
//       res.write('<p>expires in: ' + (req.session.cookie.maxAge / 1000) + 's</p>')
//       res.end()
//     } else {
//       req.session.views = 1

//       res.end('welcome to the session demo. refresh!')
//     }
//   })
app.use(express.static(path.join(__dirname, 'dist')));



app.get('/', function(req, res) {
    req.visitor.pageview("/home", "http://13.232.81.86", "landingpage").send();
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// catch 404
app.use((req, res, next) => {
    res.status(404).send('<h2 align=center>Page Not Found!</h2>');
});


// // start server
var port = process.env.NODE_ENV === 'production' ? 80 : 4000;
var server = app.listen(port, function() {
    console.log('Listening on ' + port);
});

// const server = require('http').createServer();
// const port = process.env.PORT || 4000;

// server.listen(port, function() {

// });