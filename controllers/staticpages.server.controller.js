// ./express-server/controllers/product.server.controller.js
import mongoose from 'mongoose';

//import models
import Staticpages from '../models/staticpages.server.model';
var passport = require('passport');

export const getallStaticPages = (req, res) => { 
    Staticpages.find().exec((err, pages) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error in pages' });
        }
        return res.json({ 'success': true, 'message': 'pages fetched successfully', pages });
    });
}


export const getallactiveStaticPages = (req, res) => { 
    Staticpages.find({active: true}).exec((err, pages) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error in pages' });
        }
        return res.json({ 'success': true, 'message': 'pages fetched successfully', pages });
    });
}

export const addStaticpages = (req, res) => { 
    const newStaticpages = new Staticpages(req.body);
    newStaticpages.save((err, pages) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error tod' });
        }
        return res.json({ 'success': true, 'message': 'pages added successfully', pages });
    })
}

export const updateStaticpages = (req, res) => { 
    Staticpages.findOneAndUpdate({ _id: req.body._id }, req.body, (err, pages) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error', 'error': err });
        } 
        return res.json({ 'success': true, 'message': 'Products Updated Successfully', pages });
    })
} 
 
export const getStaticpageById = (req, res) => { 

    Staticpages.find({ _id: req.params.id }).exec((err, pages) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Errord' });
        }
        if (pages.length) { 
            return res.json({ 'success': true, 'message': 'Products fetched by id successfully', pages });
        } else {
            return res.json({ 'success': false, 'message': 'Products with the given id not found' });
        }
    })
}

export const deleteStaticpages = (req, res) => {
    Staticpages.findByIdAndRemove(req.params.id, (err, pages) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error' });
        }
        return res.json({ 'success': true, 'message': 'Products Deleted Successfully', pages });
    })
}