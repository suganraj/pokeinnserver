// ./express-server/controllers/product.server.controller.js
import mongoose from 'mongoose';
import users from '../models/users.server.model';

export const updateUsers = (req, res) => { 
    users.findOneAndUpdate({ _id: req.body._id }, req.body, (err, user) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error', 'error': err });
        } 
        return res.json({ 'success': true, 'message': 'user Updated Successfully', user });
    })
}

export const updateUsersImage = (req, res) => { 
    users.findOneAndUpdate({ _id: req.body._id }, { $set:{ 'common.photoUrl': req.body.common.photoUrl }} , (err, user) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error', 'error': err });
        } 
        return res.json({ 'success': true, 'message': 'user Updated Successfully', user });
    })
}


export const updateUserspaymentMethod = (req, res) => { 
    users.findOneAndUpdate({ _id: req.body.user }, { $set:{ 'paymentMethod': req.body._id }} , (err, user) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error', 'error': err });
        } 
        return res.json({ 'success': true, 'message': 'user Updated Successfully', user });
    })
}


export const getuserpayment = (req, res) => { 
    users.find({ _id: req.body._id } , 'paymentMethod').exec((err, user) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error' });
        }
        if (user.length) {
            return res.json({ 'success': true, 'message': 'user fetched by id successfully', user });
        } else {
            return res.json({ 'success': false, 'message': 'user with the given id not found' });
        }
    })
}


export const getuser = (req, res) => {
    users.find({ _id: req.body._id }).exec((err, user) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error' });
        }
        if (user.length) {
            return res.json({ 'success': true, 'message': 'user fetched by id successfully', user });
        } else {
            return res.json({ 'success': false, 'message': 'user with the given id not found' });
        }
    })
}
 
