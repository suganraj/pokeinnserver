// ./express-server/controllers/product.server.controller.js
import mongoose from 'mongoose';

//import models
import Products from '../models/products.server.model';
import Wishlists from '../models/wishlist.model';


export const getProducts = (req, res) => {
    Products.find().exec((err, products) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error in products' });
        }
        return res.json({ 'success': true, 'message': 'products fetched successfully', products });
    });
}

export const searchProducts = (req, res) => {
    var productskip = req.body.productskip;
    var pageLimit = req.body.pageLimit;
    var searchInput = {};
    const outputVal = 'userwish ProductType coverphoto RoomType Listing Photos.filename Rooms.Beds Description.Title Pricing.Baseprice Location  UnavailabeDates ProductRating ProductRatingCount';
 
    // req.visitor.pageview("/search", "http://13.232.81.86", "seachpage").send(); 
    for (var key in req.body.searchFilter) {
        req.body.searchFilter[key] !== "" ? searchInput[key] = req.body.searchFilter[key] : null;
    }
    searchInput["status"] = "completed";
    searchInput["published"] = true;

    if (req.body.activeUser) {
        searchWishlistproducts(searchInput, outputVal, productskip, pageLimit, res, req.body.activeUser);
    } else {
        searchproducts(searchInput, outputVal, productskip, pageLimit, res);
    }
}

function searchproducts(searchInput, outputVal, productskip, pageLimit, res, wishlist) {
    Products.find(searchInput, outputVal, function (err, product) {
        if (err) return handleError(err);
        else {
            var totalrecord = product.length;
            Products.find(searchInput, outputVal, { skip: productskip, limit: pageLimit }, function (err, product) {
                if (err) return handleError(err);
                else {
                    if (wishlist) {
                         for(let i = 0; i < product.length; i++){
                             if (wishlist.indexOf(JSON.stringify(product[i]._id)) > -1) {
                                product[i].userwish = true;
                            }
                         }
                         return res.json({ 'success': true, 'message': 'Products fetched by id successfully', product, totalrecord });                      
                    } else {
                        return res.json({ 'success': true, 'message': 'Products fetched by id successfully', product, totalrecord });
                    }
                }
            });
        }
    });
}

function searchWishlistproducts(searchInput, outputVal, productskip, pageLimit, res, activeUser) {
    var userwishlist = [];

    Wishlists.find({ wishlistUser: activeUser }).populate('productlist', 'Description.Title coverphoto.filename').exec((err, wishlist) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Errord' });
        }
        if (wishlist.length) {
            for (let i = 0; i < wishlist.length; i++) {
                for (let j = 0; j < wishlist[i].productlist.length; j++) {
                    if (userwishlist.indexOf(wishlist[i].productlist[j]._id) === -1) {
                        userwishlist = [...userwishlist, JSON.stringify(wishlist[i].productlist[j]._id)];
                    }
                }
            }
            searchproducts(searchInput, outputVal, productskip, pageLimit, res, userwishlist);
        } else {
            searchproducts(searchInput, outputVal, productskip, pageLimit, res)
        }
    })


}



export const addProducts = (req, res) => {
    // 
    const newProducts = new Products(req.body);
    newProducts.save((err, product) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error tod' });
        }
        return res.json({ 'success': true, 'message': 'Products added successfully', product });
    })
}

export const updateProducts = (req, res) => {
    // 
    Products.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, product) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error', 'error': err });
        } 
        return res.json({ 'success': true, 'message': 'Products Updated Successfully', product });
    })
}

export const getProduct = (req, res) => { 

    Products.find({ _id: req.params.id }).populate('host').exec((err, product) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Errord' });
        }
        if (product.length) {
         
            return res.json({ 'success': true, 'message': 'Products fetched by id successfully', product });
        } else {
            return res.json({ 'success': false, 'message': 'Products with the given id not found' });
        }
    })
}

export const deleteProducts = (req, res) => {
    Products.findByIdAndRemove(req.params.id, (err, product) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error' });
        }
        return res.json({ 'success': true, 'message': 'Products Deleted Successfully', product });
    })
}