﻿const Admin = require('../models/admin.model.js');
var passport = require('passport');
var mongoose = require('mongoose');


exports.register = function (req, res) {
    // Validate request
    if (!req.body.firstname || !req.body.lastname || !req.body.email || !req.body.password) {
        return res.status(400).send({
            message: "Fields can not be empty"
        });
    }

    var admin = new Admin({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: req.body.password,
        role: 'subadmin',
        privilege: req.body.privilegestr
    });
 

    Admin.findOne({ email: req.body.email })
        .then(adminExists => {
            if (adminExists) {
                return res.status(500).send({
                    message: "Admin already exists"
                });
            } else {
                admin.setPassword(req.body.password);
                admin.save(function (err) {
                    var token;
                    token = admin.generateJwt();
                    res.status(200);
                    res.json({
                        "token": token
                    });
                });
            }
        });
};

// Find a single rideType with a rideTypeId
exports.findOne = (req, res) => {
    Admin.findById(req.params.adminId)
    .then(admin => {
        if(!admin) {
            return res.status(404).send({
                message: "Admin not found with id " + req.params.adminId
            });            
        }
        res.send(admin);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Admin not found with id " + req.params.adminId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving admin with id " + req.params.adminId
        });
    });
};


exports.login = function (req, res) {
    

    passport.authenticate('local', function (err, admin, info) {
        var token;
        // If Passport throws/catches an error
        if (err) { 
            res.status(404).json(err);
            return;
        }
        // If a admin is found
        if (admin) {
            token = admin.generateJwt(); 
            res.status(200);
            res.json({
                "token": token
            });
        } else {
            // If admin is not found
            res.status(401).json(info);
        }
    })(req, res);
};


// Retrieve and return all admins from the database.
exports.findAll = (req, res, next) => {
    Admin.find().exec((err, data) => {
        if (err) {
            const error = new Error('Some Error in product');
            return next(error);
        }
        return res.json({ 'success': true, 'message': 'product fetched successfully', 'data': data });
    });
}


// Delete a admin with the specified adminId in the request
exports.delete = (req, res) => {
    Admin.findByIdAndRemove(req.params.adminId)
        .then(admin => {
            if (!admin) {
                return res.status(404).send({
                    message: "Admin not found with id " + req.params.adminId
                });
            }
            res.send({ message: "Admin deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Admin not found with id " + req.params.adminId
                });
            }
            return res.status(500).send({
                message: "Could not delete note with id " + req.params.adminId
            });
        });
};

// Find a single note with a noteId
exports.findOne = (req, res, next) => {
    Admin.find({ _id: req.params.id }).exec((err, data) => {
        if (err) {
            const error = new Error('Some Error found in fetching admin');
            return next(error);
        }
        if (data.length) {
            return res.json({ 'success': true, 'message': 'admin fetched by id successfully', data });
        } else {
            return res.json({ 'success': false, 'message': 'admin with the given id not found' });
        }
    });
};

