// ./express-server/controllers/product.server.controller.js
import mongoose from 'mongoose';

//import models
import Coupon from '../models/coupon.server.model'; 

export const getallCoupons = (req, res) => { 
    Coupon.find().exec((err, coupons) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error in sliders' });
        }

        return res.json({ 'success': true, 'message': 'Coupon fetched successfully', coupons });
    });
}

export const getCouponbyId = (req, res) => {
    Coupons.find({ _id: req.params.id }).exec((err, coupons) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Errord' });
        }
        if (coupons.length) {
            return res.json({ 'success': true, 'message': 'Coupon fetched by id successfully', coupons });
        } else {
            return res.json({ 'success': false, 'message': 'Coupon with the given id not found' });
        }
    })
}


export const addCoupon = (req, res) => {
    
    const newCoupon = new Coupon(req.body);
    newCoupon.save((err, coupons) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error Found' });
        }

        return res.json({ 'success': true, 'message': 'Coupon added successfully', coupons });
    })
}

export const updateCoupon = (req, res) => {
    Coupon.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, coupons) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error', 'error': err });
        } 
        return res.json({ 'success': true, 'message': 'Coupon Updated Successfully', coupons });
    })
}

export const deleteCoupon = (req, res) => {
    Coupon.findByIdAndRemove(req.params.id, (err, coupons) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error' });
        }

        return res.json({ 'success': true, 'message': 'Coupon Deleted Successfully', coupons });
    })
}



 

 