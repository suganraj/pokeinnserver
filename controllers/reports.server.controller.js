// ./express-server/controllers/product.server.controller.js
import mongoose from 'mongoose';

//import models
import Products from '../models/products.server.model';
import users from '../models/users.server.model';
import Bookings from '../models/bookings.server.model';

export const getreports = (req, res) => {
    const reportsearch = req.body;
    var reportModel;
    var topopulate;
    var reportoutput;
    switch (reportsearch.category) {
        case 'users':
            reportModel = users;
            topopulate = '';
            reportoutput = 'status createdAt common';
            break;
        case 'rooms':
            reportModel = Products;
            topopulate = 'host';
            reportoutput = 'Listing Description createdAt published host';            
            break;
        case 'reservation':
            reportModel = Bookings;
            topopulate = 'productId  producthost traveller';
            reportoutput = 'productId  producthost traveller pricing createdAt status ';           
            break;
        default: reportModel = Bookings;
    }  
    reportModel.find({createdAt: { $gte: new Date(reportsearch.from), $lt: new Date(reportsearch.to) }} , reportoutput)
    .populate(topopulate).exec((err, reports) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error in Reports' });
        }
        return res.json({ 'success': true, 'message': 'Reports fetched successfully', reports });
    });
}


