// ./express-server/controllers/product.server.controller.js
import mongoose from 'mongoose';

//import models
import Messages from '../models/messages.server.model';
var nodemailer = require('nodemailer');
var config = require('../config.json'); 
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.mailUser,
        pass: config.mailPass
    }
});  

export const getallMessages = (req, res) => {
    Messages.find().lean().populate('from', 'common.email  common.firstName , common.lastName , common.photoUrl').populate('to', 'common.email  common.firstName , common.lastName , common.photoUrl').sort({ sendDate: -1 }).exec((err, inbox) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error in inbox data' });
        }
        return res.json({ 'success': true, 'message': 'inbox data fetched successfully', inbox });
    });
}


export const getMessages = (req, res) => {
    //to: req.body.id, 
    var searchInput;
    if (req.body.inboxtype === 'travelling') {
        searchInput = { traveller: req.body.id };
    } else if (req.body.inboxtype === 'hosting') {
        searchInput = { hoster: req.body.id };
    }
    const outputVal = 'subject from to status message sendDate bookingId traveller hoster bookingStatus';
 
    Messages.find(searchInput, outputVal).lean().
    populate('from', 'common.email  common.firstName , common.lastName , common.photoUrl').sort({ sendDate: -1 }).
    exec(function(err, message) {
        if (err) return handleError(err);
        else
            return res.json({ 'success': true, 'message': 'inbox data fetched by id successfully', message });
        // prints "The author is Ian Fleming"
    });
}






export const getBookingMessages = (req, res) => {
    
    var searchInput = { bookingId: req.body.bookingId };
    const outputVal = 'subject from to status message sendDate bookingId hoster traveller';

    Messages.find(searchInput, outputVal).lean().
    populate('from', 'common.email , common.photoUrl ,  common.firstName , common.lastName').sort({ sendDate: -1 }).
    exec(function(err, message) {
        if (err) return handleError(err);
        else
            return res.json({ 'success': true, 'message': 'inbox data fetched by id successfully', message });
        // prints "The author is Ian Fleming"
    });
}


export const replyMessages = (req, res) => {
    
    const newMessages = new Messages(req.body);
    newMessages.save((err, message) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error tod' });
        }
        return res.json({ 'success': true, 'message': 'inbox data added successfully', message });
    })
}

export const updateStatus = (req, res) => {
    
    Messages.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, product) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error', 'error': err });
        } 
        return res.json({ 'success': true, 'message': 'inbox data Updated Successfully', product });
    })
}


export const deleteMessage = (req, res) => {
    Messages.findByIdAndRemove(req.params.id, (err, inbox) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error' });
        }
        return res.json({ 'success': true, 'message': 'inbox data Deleted Successfully', inbox });
    })
}


export const sendMail = (req, res) => {
    
    const mailInfo = req.body
    var mailOptions = {
        from: 'no-reply@yourwebapplication.com',
        to: mailInfo.sendto,
        subject: mailInfo.Subject,
        text: mailInfo.message
    };
    transporter.sendMail(mailOptions, function (err) {
        if(err){ 
             }else{
                return res.json({ 'success': true, 'message': 'Message sent successfully', });
             } 
    });
}

