// ./express-server/routes/product.server.route.js
import express from 'express';

//import controller file
import * as homeslider from './controllers/homeslider.server.controller';
import * as productController from './controllers/products.server.controller';
import * as listingprops from './controllers/listingtype.controller';
import * as searchController from './controllers/search.server.controller';
import * as messageController from './controllers/message.server.controller';
import * as bookingsController from './controllers/bookings.server.controller';
import * as reviewController from './controllers/reviews.server.controller';
import * as cardController from './controllers/cards.server.controller';
import * as policyController from './controllers/policy.server.controller';
import * as mobiletokenController from './controllers/verfication.server.controller';
import * as userlistingController from './controllers/userlisting.server.controller'
import * as userController from './controllers/user.server.controller';
import * as sitesettings from './controllers/sitesettings.server.controller';
import * as commonController from './controllers/common.server.controller';
import * as wishlistController from './controllers/wishlist.server.controller';
import * as staticpageController from './controllers/staticpages.server.controller';
import * as couponController from './controllers/coupon.server.controller';
import * as reportsController from './controllers/reports.server.controller';
import * as paymentController from './controllers/payment.server.controller';

import * as adminController from './controllers/admin.server.controller';

 

// product



// get an instance of express router
const router = express.Router();






const app = express(); 
var config = require('./config.json');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

import users from './models/users.server.model';

app.set('superSecret', config.secret);

router.post('/authenticate', authenticate);

function authenticate(req, res) {
    users.findOne({
        'local.email': req.body.email
    }, function (err, user) {
        if (err) throw err;
        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (!user.isVerified) {
            res.json({ success: false, message: 'Authentication failed. User Is not verfied yet.' });
        } else if (user && user.isVerified) {
            //  check if password matches
            if (!bcrypt.compareSync(req.body.password, user.local.hash)) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });
            } else {
                // if user is found and password is right
                // create a token with only our given payload
                // we don't want to pass in the entire user since that has the password
                const payload = {
                    admin: user.common
                };
                // console.log('payload', payload, app.get('superSecret'))
                var token = jwt.sign(payload, app.get('superSecret'), {
                    expiresIn: 14440 // expires in 24 hours
                });
                user.local.hash = '';
                // console.log(user.local);
                req.session.userId = user._id;
                global.sessionUser = user._id;
                

                // req.session.login(user._id, function (err) {
                //     if (err) {
                //         return res.status(500).send("There was an error logging in. Please try again later.");
                //     }
                // });
                // const user_id = user._id;

                // req.login(user_id , function(err){
                //    console.log('hi');
                //    console.log(req.isAuthenticated());
                //    console.log('user_id' , req.user_id);
                // })
                // return the information including token as JSON
                res.json({ success: true, message: 'Enjoy your token!', token: token, res: user });
            }
        }
    });
}



module.exports = router;


function requireLogin(req, res, next) {
    if (req.session.loggedIn) {
        console.log('login')
      next(); // allow the next route to run
    } else {
      // require the user to log in
      console.log('logout')
      res.redirect("/login"); // or render a form, etc.
    }
  }






 
  

//Home Slider
router.route('/slider').get(homeslider.getHomeSliders);
router.route('/slider').post(homeslider.addHomeSlider);
router.route('/slider').put(homeslider.updateHomeSlider);
router.route('/slider/:id').delete(homeslider.deleteHomeSlider);
//Room Type
router.route('/roomtype/filtered').get(listingprops.getAdminData);
router.route('/roomtype').get(listingprops.getAll);
router.route('/roomtype').post(listingprops.addNew);
router.route('/roomtype').put(listingprops.updateData);
router.route('/roomtype/:id').delete(listingprops.deleteData);
//bathroom Type
router.route('/bathroomtype/filtered').get(listingprops.getAdminData);
router.route('/bathroomtype').get(listingprops.getAll);
router.route('/bathroomtype').post(listingprops.addNew);
router.route('/bathroomtype').put(listingprops.updateData);
router.route('/bathroomtype/:id').delete(listingprops.deleteData);
//house Type
router.route('/housetype/filtered').get(listingprops.getAdminData);
router.route('/housetype').get(listingprops.getAll);
router.route('/housetype').post(listingprops.addNew);
router.route('/housetype').put(listingprops.updateData);
router.route('/housetype/:id').delete(listingprops.deleteData);
//building size
router.route('/buildingsize/filtered').get(listingprops.getAdminData);
router.route('/buildingsize').get(listingprops.getAll);
router.route('/buildingsize').post(listingprops.addNew);
router.route('/buildingsize').put(listingprops.updateData);
router.route('/buildingsize/:id').delete(listingprops.deleteData);
//Bed Type
router.route('/bedtype/filtered').get(listingprops.getAdminData);
router.route('/bedtype').get(listingprops.getAll);
router.route('/bedtype').post(listingprops.addNew);
router.route('/bedtype').put(listingprops.updateData);
router.route('/bedtype/:id').delete(listingprops.deleteData);
//essentialamenities Type
router.route('/essentialamenities/filtered').get(listingprops.getAdminData);
router.route('/essentialamenities').get(listingprops.getAll);
router.route('/essentialamenities').post(listingprops.addNew);
router.route('/essentialamenities').put(listingprops.updateData);
router.route('/essentialamenities/:id').delete(listingprops.deleteData);
//essentialamenities Type
router.route('/houserules/filtered').get(listingprops.getAdminData);
router.route('/houserules').get(listingprops.getAll);
router.route('/houserules').post(listingprops.addNew);
router.route('/houserules').put(listingprops.updateData);
router.route('/houserules/:id').delete(listingprops.deleteData);
//safetyamenities Type  
router.route('/safetyamenities/filtered').get(listingprops.getAdminData);
router.route('/safetyamenities').get(listingprops.getAll);
router.route('/safetyamenities').post(listingprops.addNew);
router.route('/safetyamenities').put(listingprops.updateData);
router.route('/safetyamenities/:id').delete(listingprops.deleteData);
//spaces Type
router.route('/spaces/filtered').get(listingprops.getAdminData);
router.route('/spaces').get(listingprops.getAll);
router.route('/spaces').post(listingprops.addNew);
router.route('/spaces').put(listingprops.updateData);
router.route('/spaces/:id').delete(listingprops.deleteData);
//New Products
router.route('/allproducts').get(productController.getAllProducts);
router.route('/products').get(productController.getProducts);
router.route('/products').post(productController.addProducts);
router.route('/products').put(productController.updateProducts);
router.route('/products/:id').get(productController.getProduct)
router.route('/products/:id').delete(productController.deleteProducts);
//review
router.route('/review').post(reviewController.addReviews);
router.route('/getreview/:id').post(reviewController.getReviews);
router.route('/getuserreview').post(reviewController.getuserReviews);
router.route('/getReviewsByuser').post(reviewController.getReviewsByuser);
router.route('/getallreview').get(reviewController.getallReviews);
router.route('/deleteReview').get(reviewController.deleteReviews);
// payout - card details
router.route('/savecard').post(cardController.addcarddetails);
router.route('/getcard/:id').get(cardController.getCards);
router.route('/updatecard').put(cardController.updateCards);
router.route('/deletecard/:id').delete(cardController.deleteCards);
//policy
router.route('/addcancellationpolicy').post(policyController.addPolicy);
router.route('/getcancellationpolicy').get(policyController.getPolicy);
//reports
router.route('/getreports').post(reportsController.getreports);
//sitesettings
router.route('/updatesitesettings').put(sitesettings.updatesitesettings);
router.route('/getsitesettings').get(sitesettings.getsitesettings);
// servicefeee 
router.route('/getservicefees').get(sitesettings.getservicefees);
router.route('/updateservicefees').put(sitesettings.updateservicefees);
// commonController
router.route('/getcountries').get(commonController.getCountrylist);
router.route('/getcurrency').get(commonController.getCurrencylist);
router.route('/getcurrencylist').get(commonController.getCurrencyname);
router.route('/updateCurrencyName').put(commonController.updateCurrencyName);
router.route('/getAllCurrencyname').get(commonController.getAllCurrencyname);
router.route('/getAllCurrencyname').get(commonController.getAllCurrencyname);
router.route('/getip/:id').get(commonController.getIp);
//search
router.route('/search').post(searchController.searchProducts);
//wishlist
router.route('/createwishlist').post(wishlistController.addWishlist);
router.route('/getuserlist').post(wishlistController.getwishlist);
router.route('/getallwishlist').get(wishlistController.getallwishlist);
router.route('/updatewishlist').put(wishlistController.updatewishlist);
router.route('/updatewishlistName').put(wishlistController.updatewishlistName);
router.route('/getwishlistbyId').post(wishlistController.getwishlistbyId);
router.route('/deletelist/:id').delete(wishlistController.deletewishlist);
//staticpage
router.route('/getallStaticPages').get(staticpageController.getallStaticPages);
router.route('/getallactiveStaticPages').get(staticpageController.getallactiveStaticPages);
router.route('/getStaticpageById/:id').get(staticpageController.getStaticpageById);
router.route('/addStaticpages').post(staticpageController.addStaticpages);
router.route('/updateStaticpages').put(staticpageController.updateStaticpages);
router.route('/deleteStaticpages/:id').delete(staticpageController.deleteStaticpages);
//staticpage
router.route('/getallcoupon').get(couponController.getallCoupons);
router.route('/getcouponById/:id').get(couponController.getCouponbyId);
router.route('/addcoupon').post(couponController.addCoupon);
router.route('/updatecoupon').put(couponController.updateCoupon);
router.route('/deletecoupon/:id').delete(couponController.deleteCoupon);
//message
router.route('/sendMail').post(messageController.sendMail);
router.route('/allmessages').get(messageController.getallMessages);
router.route('/message').post(messageController.getMessages);
router.route('/bookingmessage').post(messageController.getBookingMessages);
router.route('/reply').post(messageController.replyMessages);
router.route('/deletemessage/:id').delete(messageController.deleteMessage);
router.route('/statuschange').put(messageController.updateStatus)
//bookings
router.route('/getbooking/:id').get(bookingsController.getbooking);
router.route('/previoustrips').post(bookingsController.previoustrips);
router.route('/currenttrips').post(bookingsController.currenttrips);
router.route('/reservation').post(bookingsController.userreservation);
router.route('/updatereservation').put(bookingsController.updateuserreservation);
router.route('/addreservation').post(bookingsController.addreservation);
router.route('/getallReservations').get(bookingsController.getallReservations);
//bookings
router.route('/savetransaction').post(bookingsController.savetransaction);
router.route('/completedtransaction').post(bookingsController.completedtransaction);
router.route('/futuretransaction').post(bookingsController.futuretransaction);
//user data
router.route('/userupdate').put(userController.updateUsers);
router.route('/userpicupdate').put(userController.updateUsersImage);
router.route('/userpaymentupdate').put(userController.updateUserspaymentMethod);

router.route('/getuser').post(userController.getuser);  
router.route('/getuserspayment').post(userController.getuserpayment);  


// hosting charge
router.route('/paymentsucess').post(paymentController.paymentsucess);
router.route('/paymentcancel').get(paymentController.paymentcancel);
router.route('/paypaypalhostingfee').post(paymentController.paypalhostfeetransaction);

//mobileverification
router.route('/generatemobileverfication').post(mobiletokenController.generatemobileToken);
router.route('/verifymobileToken').post(mobiletokenController.verifymobileToken);
router.route('/googleverification').post(mobiletokenController.googleverification);
//userlisting
router.route('/user/listing').post(userlistingController.getlistings);



//admin autentication

router.route('/admin').post(adminController.register); 
router.route('/adminlogin').post(adminController.login);
router.route('/adminfind').get(adminController.findAll);
router.route('/admin/:id').get(adminController.findOne);
router.route('/admin/:id').delete(adminController.delete);
 











var multer = require("multer");
var fs = require('fs');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        console.log('File Name', file);
        cb(null, Date.now() + '.jpg')
    }
});
var upload = multer({ storage: storage });
var productstorage = multer.memoryStorage()
var uploadProductImage = multer({ storage: productstorage });


// file upload
router.post('/upload', upload.array("myfile[]", 12), function (req, res, next) {
    console.log(req.file)
    return res.send({
        success: true,
        file: req.files
    });
});
// product image
router.post('/upload/productImage', uploadProductImage.array(), function (req, res) {
    var base64Data = req.body.image;
    const createingImageName = 'pokeprod' + Date.now() + '.png';
    fs.writeFile(__dirname + "/uploads/products/" + createingImageName, base64Data, 'base64', function (err) {
        if (err) console.log(err);
        return res.send({
            success: true,
            image: {
                filename: createingImageName,
                destination: '/uploads/products/'
            }
        });
    });
});

export default router;