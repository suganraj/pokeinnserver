﻿var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
const Admin = require('./../models/admin.model.js')


passport.use(new LocalStrategy({
    usernameField: 'email'
  },
  function(username, password, done) { 
    console.log(username , password)
    Admin.find({ email: username }, function (err, admin) {
      if (err) { return done(err); }
      // Return if admin not found in database
      if (!admin[0]) {
        return done(null, false, {
          message: 'Admin not found'
        }); 
      }
      // Return if password is wrong
      if (!admin[0].validPassword(password)) {
        return done(null, false, {
          message: 'Password is wrong'
        });
      }
      // If credentials are correct, return the admin object
      return done(null, admin[0]);
    });
  }
));