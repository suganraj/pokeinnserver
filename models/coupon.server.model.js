import mongoose from 'mongoose';
import users from './users.server.model'


var couponSchema = mongoose.Schema({
    couponName: { type: String, required: true },
    active: { type: Boolean, required: true, default: true },
    amount: { type: Number, required: true },
    createdAt: { type: Date, required: true, default: Date.now }
});

export default mongoose.model('coupon', couponSchema)