import mongoose from 'mongoose';
// import users from './users.server.model'
var Schema = mongoose.Schema;
var currencySchema = mongoose.Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },
    live: Boolean,
    base: String,
    date: String,
    timestamp: String,  
    rates: Object
});

export default mongoose.model('Currencies', currencySchema) 