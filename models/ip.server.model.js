import mongoose from 'mongoose';


var Schema = mongoose.Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },
    ip: String,
    hostname: String,
    type: String,
    continent_code: String,
    continent_name: String,
    country_code: String,
    country_name: String,
    region_code: String,
    region_name: String,
    city: String,
    zip: String,
    latitude: String,
    longitude: String,
    location: Object,
    time_zone: Object,
    currency: Object,
    connection: Object,
    security: Object
});

export default mongoose.model('ipLists', Schema)