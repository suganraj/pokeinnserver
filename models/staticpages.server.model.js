import mongoose from 'mongoose';

var staticpagesSchema = mongoose.Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },
    content: String,
    title: String,
    meta: [{
        metatagTitle: String,
        metatagContent: String
    }],
    active:{
        type: Boolean,
        default: true
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

export default mongoose.model('staticpages', staticpagesSchema)